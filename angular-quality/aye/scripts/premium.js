async function clickElement (browser, By) {
  await browser.findElement(By.id("customerName")).sendKeys("Helene Birne");
  await browser.findElement(By.id("customerId")).sendKeys("300000");
  await browser.findElement(By.css("section form .btn.btn-primary")).click();
};

module.exports = clickElement;
